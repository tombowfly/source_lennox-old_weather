/*
 * Copyright (C) 2012-2014 The CyanogenMod Project (DvTonder)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather.preference;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListAdapter;
import com.lennox.weather.R;

import com.lennox.weather.misc.WidgetUtils;

import java.util.ArrayList;
import java.util.List;

import com.lennox.preferences.PreferenceUtils;

import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishClosedListener;
import com.lennox.pollfish.PollFishHelper;

import com.google.ads.AdsHelper;

public class Preferences extends PreferenceActivity implements PollfishSurveyReceivedListener, PollfishOpenedListener,
        PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener, PollfishClosedListener {

    private boolean mDarkTheme;

    protected final Handler mHandler = new Handler();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PollFishHelper.onCreate(this);
        AdsHelper.onCreate(this);

        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preferences_headers, target);
        PreferenceUtils.setAboutHeader(target, this);
        AdsHelper.adBanner(this, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        ActionBar mActionBar = getActionBar();
        // Show up navigation and hide 'done' button when not triggered from adding a new widget
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
            MenuItem item = menu.findItem(R.id.menu_done);
            item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_done:
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PollFishHelper.onResume(this);
        AdsHelper.onResume(this, mHandler);
    }

    @Override
    protected void onPause(){
        super.onPause();
        AdsHelper.onPause(this, mHandler);
    }

    @Override
    public void setListAdapter(ListAdapter adapter) {
        if (adapter == null) {
            super.setListAdapter(null);
        } else {
            List<Header> headers = PreferenceUtils.getHeadersFromAdapter(adapter);
            super.setListAdapter(new PreferenceUtils.HeaderAdapter(this, headers));
        }
    }

    @Override
    public void onHeaderClick(Header header, int position) {
        AdsHelper.onResume(this, mHandler);
        super.onHeaderClick(header, position);
    }

    @Override
    public void onBackPressed() {
        AdsHelper.onBackPressed(this);
    }

    /**
     * This is required to be able to build with API level 19
     */
    @SuppressLint("Override")
    @Override
    public boolean isValidFragment(String fragmentName) {
        // Assume a valid fragment name at all times
        return true;
    }

    // PollFish Helpers

    @Override
    public void onPollfishSurveyReceived(boolean shortSurveys, int surveyPrice) {
        PollFishHelper.onPollfishSurveyReceived(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        PollFishHelper.onPollfishSurveyNotAvailable(this);
    }

    @Override
    public void onPollfishSurveyCompleted(boolean shortSurveys , int surveyPrice) {
        PollFishHelper.onPollfishSurveyCompleted(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishOpened () {
        PollFishHelper.onPollfishOpened(this);
    }

    @Override
    public void onPollfishClosed () {
        PollFishHelper.onPollfishClosed(this);
    }

}
