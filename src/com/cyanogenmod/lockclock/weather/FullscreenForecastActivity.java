package com.lennox.weather.weather;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.lennox.weather.misc.Constants;
import com.lennox.weather.misc.Preferences;
import com.lennox.weather.misc.WidgetUtils;
import com.lennox.weather.weather.WeatherInfo.DayForecast;
import com.lennox.weather.R;

import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishClosedListener;
import com.lennox.pollfish.PollFishHelper;

import com.google.ads.AdsHelper;

public class FullscreenForecastActivity extends Activity
        implements PollfishSurveyReceivedListener, PollfishOpenedListener,
        PollfishSurveyNotAvailableListener, PollfishSurveyCompletedListener, PollfishClosedListener {

    private static final String TAG = "FullscreenForecastActivity";

    private BroadcastReceiver mUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Stop the animation
            if (mRefreshItem != null) {
                View actionView = mRefreshItem.getActionView();
                if (actionView != null) {
                    actionView.clearAnimation();
                }
                mRefreshItem.setActionView(null);
            }

            if (!intent.getBooleanExtra(WeatherUpdateService.EXTRA_UPDATE_CANCELLED, false)) {
                refreshView();
            }
        }
    };

    WeatherInfo mWeatherInfo = null;

    private SoundPool mSoundPool;
    private int mConditionSound;

    private TextView currentText;
    private TextView lowHighText;
    private TextView conditionText;
    private TextView dateText;
    private TextView dayText;

    private TextView humidityText;
    private TextView windSpeedText;
    //private TextView windChillText;
    private TextView visibilityText;
    private TextView pressureText;
    private TextView sunriseText;
    private TextView sunsetText;

    private TextView weatherSource;

    protected final Handler mHandler = new Handler();

    protected MenuItem mRefreshItem = null;

    @SuppressLint("InlinedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialiseView();

        PollFishHelper.onCreate(this);
        AdsHelper.onCreate(this);
        AdsHelper.adBanner(this, false);
    }

    @Override
    protected void onPause(){
        super.onPause();
        AdsHelper.onPause(this, mHandler);
        try {
            unregisterReceiver(mUpdateReceiver);
        } catch (IllegalArgumentException e) {
            android.util.Log.d(TAG, "Receiver not registered?");
            e.printStackTrace();
        }
        if (mSoundPool != null ) {
            mSoundPool.release();
        }
    }

    @Override
    public void onBackPressed() {
        AdsHelper.onBackPressed(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        PollFishHelper.onResume(this);
        AdsHelper.onResume(this, mHandler);
        registerReceiver(mUpdateReceiver, new IntentFilter(WeatherUpdateService.ACTION_UPDATE_FINISHED));
        refreshView();
    }

    public void initialiseView() {
        setContentView(R.layout.fullscreen_forecast_activity);

        currentText = (TextView)findViewById(R.id.current_temperature);
        lowHighText = (TextView)findViewById(R.id.minimum_maximum_temperature);
        conditionText = (TextView)findViewById(R.id.current_condition);
        dateText = (TextView)findViewById(R.id.current_date);
        dayText = (TextView)findViewById(R.id.current_day);

        humidityText = (TextView)findViewById(R.id.current_humidity);
        windSpeedText = (TextView)findViewById(R.id.current_wind_speed);
        //windChillText = (TextView)findViewById(R.id.current_wind_chill);
        //visibilityText = (TextView)findViewById(R.id.current_visibility);
        //pressureText = (TextView)findViewById(R.id.current_pressure);
        sunriseText = (TextView)findViewById(R.id.sunrise);
        sunsetText = (TextView)findViewById(R.id.sunset);

        weatherSource = (TextView) findViewById(R.id.weather_source);
    }

    public void refreshView() {
        // Get the forecasts data
        mWeatherInfo = Preferences.getCachedWeatherInfo(this);
        if (mSoundPool != null ) {
            mSoundPool.release();
        }
        if (mWeatherInfo == null ) {
            Intent intent = new Intent(this, com.lennox.weather.preference.Preferences.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            mSoundPool = new SoundPool(15, AudioManager.STREAM_SYSTEM, 0);
            mSoundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    if ( Preferences.playWeatherSound(FullscreenForecastActivity.this) ) {
                        soundPool.play(mConditionSound, 1.0f, 1.0f, 0, 0, 1);
                    }
                }
            });
            setupInfoView();
            setupForecastView();
        }
    }

    private void setupInfoView() {
        mConditionSound = mSoundPool.load(this, mWeatherInfo.getConditionSound(), 1);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        sdf = new SimpleDateFormat("dd MMMM");
        String currentDate = sdf.format(d);

        String lowHigh = mWeatherInfo.getFormattedLow() + " | " + mWeatherInfo.getFormattedHigh();
        if (Preferences.invertLowHighTemperature(this)) {
            lowHigh = mWeatherInfo.getFormattedHigh() + " | " + mWeatherInfo.getFormattedLow();
        }

        setTitle(mWeatherInfo.getCity());

        currentText.setText(mWeatherInfo.getFormattedTemperature());
        lowHighText.setText(lowHigh);
        conditionText.setText(mWeatherInfo.getCondition());
        dateText.setText(currentDate);
        dayText.setText(dayOfTheWeek);

        humidityText.setText(getString(R.string.humidity) +
                             ": " + mWeatherInfo.getFormattedHumidity());
        windSpeedText.setText(getString(R.string.wind) +
                             ": " + mWeatherInfo.getFormattedWindSpeed() +
                             " " + mWeatherInfo.getWindDirection());
        //windChillText.setText(mWeatherInfo.getWindChill());
        //visibilityText.setText(getString(R.string.visibility) +
        //                     ": " + mWeatherInfo.getFormattedVisibility());
        //pressureText.setText(getString(R.string.pressure) +
        //                     ": " + mWeatherInfo.getFormattedPressure() + " (" +
        //                     mWeatherInfo.getPressureRisingCharacter() + ")");
        //sunriseText.setText(getString(R.string.sunrise) +
        //                     ": " + mWeatherInfo.getSunrise());
        //sunsetText.setText(getString(R.string.sunset) +
        //                     ": " + mWeatherInfo.getSunset());

        weatherSource.setText(Preferences.weatherProvider(this).getNameResourceId());
        weatherSource.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(Preferences.weatherProviderUrl(FullscreenForecastActivity.this)));
                startActivity(intent);
            }
        });

        ImageView backgroundImage = (ImageView) findViewById(R.id.image_background);
        backgroundImage.setImageResource(mWeatherInfo.getConditionBackground());
    }

    private void setupForecastView() {
        // Get things ready
        LinearLayout forecastView = (LinearLayout) findViewById(R.id.fourDayView);
        // Build the forecast panel
        forecastView.removeAllViews();
        ForecastBuilder.buildSmallPanel(this, forecastView, mWeatherInfo);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
        case R.id.menu_item_settings:
            Intent intent = new Intent(this, com.lennox.weather.preference.Preferences.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            break;
        case R.id.menu_item_refresh:
            // Setup anim with desired properties and start the animation
            mRefreshItem = item;
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ImageView iv = (ImageView) inflater.inflate(R.layout.refresh_rotate, null);
            Animation rotation = AnimationUtils.loadAnimation(this, R.anim.clockwise_rotate);
            iv.startAnimation(rotation);
            mRefreshItem.setActionView(iv);
            Intent i = new Intent(this, WeatherUpdateService.class);
            i.setAction(WeatherUpdateService.ACTION_FORCE_UPDATE);
            startService(i);
            break;
        case android.R.id.home:
            // Action bar home button selected
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // PollFish Helpers

    @Override
    public void onPollfishSurveyReceived(boolean shortSurveys, int surveyPrice) {
        PollFishHelper.onPollfishSurveyReceived(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishSurveyNotAvailable() {
        PollFishHelper.onPollfishSurveyNotAvailable(this);
    }

    @Override
    public void onPollfishSurveyCompleted(boolean shortSurveys , int surveyPrice) {
        PollFishHelper.onPollfishSurveyCompleted(this, shortSurveys, surveyPrice);
    }

    @Override
    public void onPollfishOpened () {
        PollFishHelper.onPollfishOpened(this);
    }

    @Override
    public void onPollfishClosed () {
        PollFishHelper.onPollfishClosed(this);
    }

}
