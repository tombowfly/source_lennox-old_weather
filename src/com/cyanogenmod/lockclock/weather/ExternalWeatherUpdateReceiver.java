package com.lennox.weather.weather;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ExternalWeatherUpdateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if ("com.lennox.weather.action.REQUEST_WEATHER_UPDATE".equals(action)) {
            context.startService(new Intent().setClass(context, WeatherUpdateService.class));
        }
    }

}
